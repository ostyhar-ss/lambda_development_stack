CREATE TABLE `icecream` (
    `id` INT(6) UNSIGNED AUTO_INCREMENT PRIMARY KEY,
    `flavour` VARCHAR(255) NOT NULL,
    `serving_size` INT NOT NULL,
    `carbohydrate_portions` FLOAT(3, 2) NOT NULL
);