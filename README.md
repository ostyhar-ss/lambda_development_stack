# Python Environment Setup #

### Launching the Python environment ###
```
docker-compose pull
docker-compose up -d
docker-compose exec python bash
```
### Install dependencies  ###
```
pip install -r requirements.txt
```
### Run unit tests ###
```
tox
```

# Building the Lambda #
### After a code change the lambda workspace will need to be restarted. ###
```
docker-compose up -d --build workspace
```
### This can be automated on MacOS by running entr from the project root directory. ###

```
brew install entr
```
### Monitor the source directory ###
```
find python | entr -p docker-compose up -d --build workspace
```

# Invoking The Lambda #

Requests are accepted on this endpoint in MSK format.
```
http://localhost:9001/2015-03-31/functions/myfunction/invocations
```

Or triggered from the `vanilla_lambda_input` topic.

```
kafkacat -b localhost:9093 -t vanilla_lambda_input -P

{"flavour":"vanilla","serving_size":"60","carbohydrate_portions":"1.0"}

Ctrl+D
```

# MSK Guide

## Adding MSK Triggers

To dynamically manage MSK triggers (what Kafka topic to consume from) you need to add the file `msk-events.txt` to the root of your lambda function, the lambda pipeline will then read this file in and set up the triggers up for you.

The file is structured as follows
`{name_of_topic}:{LATEST|TRIM_HORIZON}\n`

-   **Latest**  – Process new records that are added to the stream.
-   **Trim horizon**  – Process all records in the stream

Example file registering 2 triggers:
```
topic1:LATEST
topic2:TRIM_HORIZON
```
If you no longer need a trigger for topic2 you can delete the line and redeploy and the tigger will be removed.

## Consuming MSK Payloads

In your handler function, the event is passed as the first argument. MSK by default will send a batch of events as an array of base64 encoded JSON strings. The following code should be used to handle this

```python
def handlerFunction(event, context):
	for records in event["records"].values():
		for  record  in  records:
			payload = base64.b64decode(record["value"]).decode('UTF-8')
			jsonEvent = json.loads(payload)
			doSomethingWithTheEvent(jsonEvent, context)
	return
```
