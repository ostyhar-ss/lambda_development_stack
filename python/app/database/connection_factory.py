import mysql.connector
import os

from app.exceptions.database_exception import DatabaseException

class ConnectionFactory:
    def __init__(self):
        self.host = os.getenv("DB_HOST", "")
        self.user = os.getenv("DB_USERNAME", "")
        self.password = os.getenv("DB_PASSWORD", "")
        self.schema = os.getenv("DB_SCHEMA", "")

    def getConnection(self):
        try:
            return mysql.connector.connect(
                host = self.host,
                user = self.user,
                passwd = self.password,
                database = self.schema
            )
        except mysql.connector.Error as databaseError:
            raise DatabaseException from databaseError       