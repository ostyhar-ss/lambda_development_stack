class IceCreamDao:
    def __init__(self, connection):
        self.connection = connection
    
    def get(self, id):
        pass

    def getAll(self):
        statement = "SELECT * FROM `icecream`"
        self.connection.cursor().execute(statement)

        return self.connection.cursor().fetchAll()

    def save(self, iceCream):
        statement = "INSERT INTO `icecream` (`flavour`, `serving_size`, `carbohydrate_portions`) VALUES (%s, %s, %s)"
        values = (iceCream.get("flavour"), iceCream.get("serving_size"), iceCream.get("carbohydrate_portions"))

        cursor = self.connection.cursor()
        cursor.execute(statement, values)
        rowcount = cursor.rowcount

        self.connection.commit()

        return rowcount == 1

    def update(self, id, parameters):
        pass

    def delete(self, id):
        pass