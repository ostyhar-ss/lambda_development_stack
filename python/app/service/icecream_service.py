from app.database.connection_factory import ConnectionFactory
from app.database.icecream_dao import IceCreamDao
from kafka import KafkaProducer
from app.exceptions.database_exception import DatabaseException

import json
import os

class IceCreamService:
    def addFlavour(self, data):
        try:
            iceCreamDao = IceCreamDao(ConnectionFactory().getConnection())
            producer = KafkaProducer(bootstrap_servers=[os.getenv("KAFKA_BROKERS", "")])
            producer.send('vanilla-lambda_output', json.dumps(data).encode('utf-8'))
            return iceCreamDao.save(data)
        except DatabaseException as databaseError:
             print("Error writing to database: ", databaseError)    
