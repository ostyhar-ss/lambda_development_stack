import base64
import json
from app.service.icecream_service import IceCreamService

def lambda_handler(event, context):
    for records in event["records"].values():
        for record in records:
            print("Event recieved on topic: " + record["topic"])
            payload = base64.b64decode(record["value"]).decode('UTF-8')
            jsonEvent = json.loads(payload)
            print("Formatted event payload: " + json.dumps(jsonEvent, indent=2))
            IceCreamService().addFlavour(jsonEvent) 