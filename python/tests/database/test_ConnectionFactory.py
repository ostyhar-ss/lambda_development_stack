from unittest import TestCase, mock
from unittest.mock import MagicMock
from app.database.connection_factory import ConnectionFactory
from app.exceptions.database_exception import DatabaseException

import mysql.connector

class test_ConnectionFactory(TestCase):

    @mock.patch("app.database.connection_factory.mysql.connector.connect")
    def test_getConnection(self, mockConnect):
        mockConnect.return_value = MagicMock(name='db_connection', return_value = "")
        connection = ConnectionFactory().getConnection()

        assert mockConnect is mysql.connector.connect
        self.assertEqual(mockConnect.return_value, connection)
        mockConnect.assert_called_once()
    
    @mock.patch("app.database.connection_factory.mysql.connector.connect")
    def test_getConnectionCantConnect(self, mockConnect):
        mockConnect.return_value = MagicMock(name='db_connection', return_value = "")
        mockConnect.side_effect = mysql.connector.Error("Test DB error")
        with self.assertRaises(DatabaseException):
            ConnectionFactory().getConnection()