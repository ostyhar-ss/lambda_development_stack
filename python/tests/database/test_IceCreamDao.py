from unittest.mock import MagicMock
from unittest import TestCase, mock
from app.database.icecream_dao import IceCreamDao

class test_IceCreamDao(TestCase):

    @mock.patch("mysql.connector.connect")
    def test_save(self, mockConnect): 
        mockConnect.commit = MagicMock(name='db_commit', return_value=True)
        mockConnect.cursor = MagicMock(name='db_cursor')
        mockConnect.cursor.return_value.rowcount = 1

        dao = IceCreamDao(mockConnect)
        result = dao.save({"flavour":"vanilla", "serving_size":"60", "carbohydrate_portions": "1.0"})

        mockConnect.commit.assert_called_once()
        mockConnect.cursor.return_value.execute.assert_called_once_with(
            'INSERT INTO `icecream` (`flavour`, `serving_size`, `carbohydrate_portions`) VALUES (%s, %s, %s)', ('vanilla', '60', '1.0')
        )

        self.assertTrue(result)