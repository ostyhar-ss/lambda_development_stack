from unittest import TestCase, mock
from app.service.icecream_service import IceCreamService
import json

class test_IceCreamServiceTest(TestCase):

    @mock.patch("app.service.icecream_service.IceCreamDao")
    @mock.patch("app.service.icecream_service.ConnectionFactory")
    @mock.patch("app.service.icecream_service.KafkaProducer")
    def test_addFlavour(self, mockProducer, mockConnection, mockDao):
        event = {"flavour":"vanilla", "serving_size":"60", "carbohydrate_portions":"1.0"}

        iceCreamDao = mockDao()
        iceCreamDao.save.return_value = True

        kafka = mockProducer()
        mockProducer.send.return_value = True

        result = IceCreamService().addFlavour(event)
        self.assertTrue(result)
        assert mockConnection.called

        iceCreamDao.save.assert_called_once_with(event)
        kafka.send.assert_called_once_with('vanilla-lambda_output', json.dumps(event).encode('utf-8'))
